  import java.util.Scanner;
  /* Whittney Moore
  10/04/17
*/
class ExerciseTwo{
  public static void main(String []args){
    String[] lookUpTable = {"0000", "0001", "0010", "0011", "0100", "0101", "0110",
    "0111", "1000"};
      Scanner input = new Scanner(System.in);
      int numberGiven;
      do{
        System.out.println("Enter a number from 0 to 8.");
        numberGiven = input.nextInt();

        if (numberGiven <=8 && numberGiven >= 0){
          String binaryVersion = lookUpTable[numberGiven];
          System.out.println("The number " + numberGiven + " is " + binaryVersion + " in binary!");
        }
      }
      while (numberGiven <=8 && numberGiven >= 0);
  }
}
