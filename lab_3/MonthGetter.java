import java.util.Scanner;
/*
Whittney Moore
9/13/17
*/
public class MonthGetter{
  public static void main(String [] args){
    // Create a new Scannerinstance to get command line input
    Scanner input = new Scanner(System.in);
    int month;

    do {
      System.out.println("Enter a month (1-12).");
      month = input.nextInt();
    }while (!((month >= 1) && (month <=12)));
    System.out.println("You picked month " + month);

  } //end main
} //end class
