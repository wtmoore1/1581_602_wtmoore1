// Whittney Moore 9/13/17 Number VALIDATOR
import java.util.Scanner;
public class NumberValidator{
  public static void main(String [] args){
    Scanner input = new Scanner(System.in);
    int integer;

// askes for a number from the user
    System.out.println("Enter a number between 0 and 10.");
    integer = input.nextInt();
    if((integer > 0) && (integer < 10)){
      System.out.println("Valid number.");
    }
      else {
        System.out.println("Invalid number.");
    }
    // asks for number divisible by either 2 or 3 then  validates

    System.out.println("Enter a number that is divisible by either 2 or 3.");
    integer = input.nextInt();
    if (((integer % 2) == 0) || ((integer % 3) == 0)){
      System.out.println("Valid number.");
    }
    else {
      System.out.println("Invalid number.");
    }

    // asks for a number that is negative and even or positive and odd
    System.out.println("Enter a number that is negative and even or positive and odd.");
    integer = input.nextInt();
    if(((integer < 0) && ((integer%2)== 0)) || (( integer > 0) && ((integer % 2) == 1)) ){
      System.out.println("Valid Number.");
    }

    else {
      System.out.println("Invalid number.");
    }
// askes for a number divisible by either two or five but not both
      System.out.println("Enter a number that is divisible by 2 or 5 but not both.");
      integer = input.nextInt();

      if((((integer % 2) == 0) || ((integer % 5) == 0)) && !((integer % 10) == 0)){
        System.out.println("Valid number.");
      }
      else{
        System.out.println("Invalid Number.");
      }
      // asks for a number that is positive and odd.
      System.out.println("Enter a number that is odd and positive.");
      integer = input.nextInt();

      if(!((integer % 2) ==0 ) && !(integer <=0)){
        System.out.println("Valid number.");
      }
      else{
        System.out.println("Invalid Number");
      }
    } // end main
} // end class
