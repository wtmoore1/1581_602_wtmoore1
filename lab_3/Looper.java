import java.util.Scanner;
/*
Whittney Moore
9/13/17
*/

public class Looper{
  public static void main(String [] args){
     Scanner input = new Scanner(System.in);
     int iterations;

     System.out.println("How many iterations would you like?");
     iterations = input.nextInt();

     for(int counter = 0; counter < iterations; counter ++){
       System.out.println("Counter = " + counter);
     }// forloop
  } // end main
}// end class
