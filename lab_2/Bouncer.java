import java.util.Scanner;
/* Bouncer Assignment
* Whittney Moore
* 083017
*/
class Bouncer{
  public static void main(String[] args){
    Scanner input = new Scanner (System.in);
    int numberInLine;
    System.out.println("How many people are in line?");
    numberInLine = input.nextInt();
    while (numberInLine > 0){
        int age;
        System.out.println("I.D., please!");
        System.out.println("How old are you?");
        age = input.nextInt();
        if (age < 18){
          System.out.println("Sorry, kid. You're not old enough.");
        }
        else if (age >=18 && age <21){
          System.out.println("Come in, but NO DRINKS.");
        }
        else{
          System.out.println("Help yourself.");
        }
        System.out.println("Nonetheless, have a good night!");
        numberInLine--;
        }
        System.out.println("Well, I guess I'll go take a nap then.");
  }
} // end class Bouncer
