public interface Moveable{
void moveVertical(double distance);
void moveHorizontal(double distance);
}
